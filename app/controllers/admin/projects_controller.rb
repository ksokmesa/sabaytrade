class Admin::ProjectsController < ApplicationController
  before_filter :authenticate_user!
  before_filter :validate_user

	def index
    @projects = Project.all
    render layout: "admin"
  end

  def new
    @project = Project.new
  end

  def edit
    @project = Project.find_by_id params[:id]
    @bid_projects = @project.bid_projects
    render layout: "admin"
  end

  def show
    @project = Project.find_by_id params[:id]
    render layout: "admin"
  end

  def update
    @project = Project.find_by_id params[:id]
    if @project.update_attributes!(params[:project])
      redirect_to admin_project_path(@project)
    else
      render :edit
    end
  end

  def destroy
    @project = Project.find_by_id params[:id]
    if @project.destroy!
      redirect_to admin_projects_path()
    else
      flash[:error] = "Failed to delete project."
      redirect_to admin_projects_path()
    end
  end

  def create
    if params[:project]["end_date"]
      end_date = DateTime.strptime(params[:project]["end_date"], "%m-%d-%Y").to_date.to_s 
      params["project"].delete("end_date")
    else
      end_date = params[:project]["end_date"]
    end
    @project = current_user.projects.new(params[:project])
    @project.end_date = end_date
    @project.start_date = DateTime.now();
    @project.save!
  end

  def assign_consultant
    @project = Project.find_by_id params[:id]
    if @project.update_attributes!(params[:project])
      redirect_to admin_project_path(@project)
    else
      render :edit
    end
  end

  private

  def validate_user
    head :forbidden unless current_user.type.name == 'admin'
  end

end