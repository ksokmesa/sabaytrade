class FreelancersController < ApplicationController
	before_filter :authenticate_user!, :except => [:show]
  before_filter :validate_user, :except => [:show]

  def index
    @user = current_user
    @latest_projects = Project.order(created_at: :desc).limit(10)
  end

  def view_profile
    @user = current_user
    @latest_projects = Project.order(created_at: :desc).limit(10)
    render :index
  end

  def edit
  	@profile = current_user.profile
  	unless @profile.dob
      @profile.dob = DateTime.now().strftime("%d-%m-%Y")
    else
      @profile.dob = @profile.dob.strftime("%d-%m-%Y")
    end
  end

  def show
    @profile = User.find_by_id(params[:id]).profile
    unless @profile.dob
      @profile.dob = DateTime.now()
    end
  end

  def update
  	@profile = current_user.profile
    params[:profile]["photo_path"] = save_photo("fileupload") if params[:fileupload]
    old_profile_photo = @profile.photo_path
  	if(@profile.update_attributes(params[:profile]))
      delete_photo([old_profile_photo]) if old_profile_photo
      @profile.dob = DateTime.strptime(params["profile"]["dob"], "%d-%m-%Y")  unless params["profile"]["dob"].strip.empty?
      @profile.save
      flash[:notice] = "You have successfully updated your profile."
      redirect_to freelancers_path
    else
      render :edit
    end
  end

  private

  def delete_photo photo_name
    UploadUtils.purgePhotos(photo_name)
  end  

  def save_photo file_param
    file = params[file_param] || {}
    file_name = DateTime.now().to_i.to_s + file.original_filename 
    file_data = File.read(file.tempfile)
    UploadUtils.uploadSingleFile file_name, file_data if file_data
    file_name
  end

  def validate_user
    head :forbidden unless current_user.type.name == 'freelancer' || current_user.type.name == 'employer'
  end

end