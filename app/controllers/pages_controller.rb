class PagesController < ApplicationController
	def index
    if current_user && !params[:explicit]
      if current_user.type.name == "freelancer"
        redirect_to "/freelancer/#{current_user.display_name_no_space}"
      elsif current_user.type.name == "employer"
        redirect_to "/employer/#{current_user.display_name_no_space}"
      elsif current_user.type.name == "admin"
        redirect_to admin_projects_path
      end
    else
      redirect_to welcome_path 
    end
  end

  def welcome
  end

  def contactus
    @contact_us = ContactUs.new
  end

  def howitwork
  end

  def beanemployer
  end

  def beafreelancer
  end
end