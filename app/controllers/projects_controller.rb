class ProjectsController < ApplicationController
  before_filter :authenticate_user!, :except => [:show, :index]
  before_filter :validate_user, :except => [:show, :index, :post_bid, :detail]

	def index
    @active_projects = Project.all
    @projects = Project.all.paginate(:page => params[:page], :per_page => 10)
  end

  def new
    @project = Project.new
  end

  def create
    if params[:project]["end_date"]
      end_date = DateTime.strptime(params[:project]["end_date"], "%m-%d-%Y").to_date.to_s 
      params["project"].delete("end_date")
    else
      end_date = params[:project]["end_date"]
    end
    @project = current_user.projects.new(params[:project])
    @project.end_date = end_date
    @project.start_date = DateTime.now();
    if @project.save!
      redirect_to employers_path
    end
  end

  def edit
    @project = current_user.projects.find_by_id params[:id]
    @bid_projects = @project.bid_projects
  end

  def show
    @project = Project.find_by_id params[:id]
  end

  def detail
    @project = Project.find_by_id params[:id]
  end

  def update
    @project = Project.find_by_id params[:id]
    if @project.update_attributes!(params[:project])
      redirect_to project_path(@project)
    else
      render :edit
    end
  end

  # def show
  #   @project = Project.find_by_id params[:id]
  #   @bid = BidProject.find_by_project_id_and_user_id(params[:id],current_user.id)
  #   unless @bid
  #     @bid = BidProject.new
  #   end
  # end

  def post_bid
    @project = Project.find_by_id params[:id]
    @bid = BidProject.find_by_project_id_and_user_id(params[:id],current_user.id)
    unless @bid
      if params["bid_project"]["cost"].to_i < @project.lowest_bid
        @bid = BidProject.create!(:project => @project, :user => current_user, :cost => params["bid_project"]["cost"])
        redirect_to project_path(@project)
      else
        @project.errors.add("Bid", " was not accepted.Please input your bid below $#{@project.lowest_bid}.")
        render :show
      end  
    else
      @project.errors.add("Project", "Could not find project to bid.")
      render :show
    end
  end

  def assign_consultant
    @project = current_user.projects.find_by_id params[:id]
    if @project.update_attributes!(params[:project])
      redirect_to project_path(@project)
    else
      render :edit
    end
  end

  private

  def validate_user
    head :forbidden unless (current_user.type.name == 'employer' || current_user.type.name == 'admin')
  end

end