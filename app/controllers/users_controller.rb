class UsersController < ApplicationController  
  before_filter :authenticate_user!, :except => [:register_employer, :save_employer, :register_freelancer, :save_freelancer]
  def register_employer
    @user = User.new()
  end

  def save_employer
    params[:user][:type_id] = Type.find_by_name("employer").id
    @user = User.create(params[:user])
    if(params["user"]["password"] != "" and params["user"]["password_confirmation"] != "")
      if(params["user"]["password"] == params["user"]["password_confirmation"])
        if @user.errors.messages.empty?
          if @user.save!
            sign_in @user
            redirect_to edit_employer_path(@user)
          else
            render :register_employer
          end
        else
          render :register_employer
        end
      else
        render :register_employer
      end
    else
      render :register_employer
    end
  end

  def register_freelancer
    @user = User.new()
  end

  def save_freelancer
    params[:user][:type_id] = Type.find_by_name("freelancer").id
    @user = User.create(params[:user])
    if(params["user"]["password"] != "" and params["user"]["password_confirmation"] != "")
      if(params["user"]["password"] == params["user"]["password_confirmation"])
        if @user.errors.messages.empty?
          if @user.save!
            sign_in @user
            redirect_to edit_freelancer_path(@user)
          else
            render :register_employer
          end
        else
          render :register_employer
        end
      else
        render :register_employer
      end
    else
      render :register_employer
    end
  end

  def get_position
    render :json => Industry.selected_position(params[:industry])
  end
end