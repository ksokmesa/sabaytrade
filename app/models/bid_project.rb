class BidProject < ActiveRecord::Base
  attr_accessible :cost, :user, :project
  belongs_to :project
  belongs_to :user

end
