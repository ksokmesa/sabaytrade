class ContactUs < ActiveRecord::Base 
  validates_presence_of :first_name
  validates_presence_of :last_name
  validates_presence_of :email
  validates_presence_of :subject
  validates_presence_of :description

  attr_accessible :first_name
  attr_accessible :last_name
  attr_accessible :email
  attr_accessible :subject
  attr_accessible :description

end