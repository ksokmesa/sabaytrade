class Industry

    # INDUSTRY = {
    #     "CopyWriting" => "CopyWriting",
    #     "Graphic Design and ICT Materials" => "Graphic Design and ICT Materials",
    #     "Web Development" => "Web Development",
    #     "Translation" => "Translation"
    # }

    INDUSTRY = {
        "CopyWriting" => "CopyWriting"
    }

	POSITION_DEV = { 
    	"Application Developer" => "Application Developer",    
        "Computer and Information Systems Manager" => "Computer and Information Systems Manager",
        "Computer Systems Manager" => "Computer Systems Manager",
        "Front End Developer" => "Front End Developer",
        "Help Desk Technician" => "Help Desk Technician",
        "IT Support Specialist" => "IT Support Specialist",
        "Java Developer" => "Java Developer",
        "Junior Software Engineer" => "Junior Software Engineer",
        ".NET Developer" => ".NET Developer",
        "Senior Software Engineer" => "Senior Software Engineer",
        "Senior Systems Software Engineer" => "Senior Systems Software Engineer",
        "Senior Web Developer" => "Senior Web Developer",
        "Software Engineer" => "Software Engineer",
        "Support Specialist" => "Support Specialist",
        "Web Administrator" => "Web Administrator",
        "Web Developer" => "Web Developer",
        "Webmaster" => "Webmaster"
    }

    POSITION_COPY = { 
        "Marketing Article" => "Marketing Article",    
        "General Essay" => "General Essay"
    }

    POSITION_MARKETING = { 
        "Marketing Material (Flyers, Banners, Posters, NameCard...)" => "Marketing Material (Flyers, Banners, Posters, NameCard...)",    
        "Communication MaterialsDEV" => "Communication Materials"
    }

    POSITION_TRANSLATE = { 
        "Khmer - English" => "Khmer - English",    
        "English - Khmer" => "English - Khmer ",
        "Chinese - English" => "Chinese - English",
        "Chinese - Khmer" => "Chinese - Khmer",
        "Japanese - Khmer" => "Japanese - Khmer",
        "Thai - Khmer" => "Thai - Khmer"
    }

    def self.selected_position industry
        case industry
        when "CopyWriting"
            return Industry::POSITION_COPY
        when "Graphic Design and ICT Materials"
            return Industry::POSITION_MARKETING
        when "Web Development"
            return Industry::POSITION_DEV
        when "Translation"
            return Industry::POSITION_TRANSLATE
        else
            return Industry::POSITION_COPY
        end
    end

end