class Profile < ActiveRecord::Base

	attr_accessible :user
	attr_accessible :first_name
    attr_accessible :last_name
    attr_accessible :sex
    attr_accessible :photo_path
    attr_accessible :skill
    attr_accessible :address
    attr_accessible :phone_number
    attr_accessible :dob
    attr_accessible :description
    attr_accessible :company
    belongs_to :user

	SEX = {"female" => "Female", "male" => "Male"}

end