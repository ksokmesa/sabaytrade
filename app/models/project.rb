class Project < ActiveRecord::Base
	validates_presence_of :name, :description, :budget, :end_date, :start_date, :industry, :sub_industry, :status
	attr_accessible :name, :status, :industry, :sub_industry,  :description, :budget, :end_date, :start_date, :consultant_id, :consultant_cost, :tags

	belongs_to :user
	has_many :bid_projects, dependent: :destroy

	STATUS = {
    "Bidding" => "Bidding",
    "On going" => "On going",
    "Dalayed" => "Dalayed",
    "Finished" => "Finished"
  }

	def consultant
		User.find_by_id(self.consultant_id)
	end

	def consultant_name
		user = User.find_by_id(self.consultant_id)
		return user.display_name if user 
    ""
	end

	def employer_name
		return self.user.display_name if self.user 
    ""
	end

	def lowest_bid
		lowest = self.budget
		self.bid_projects.each do |bp|
			if bp.cost and lowest.to_i > bp.cost.to_i
				lowest = bp.cost.to_i
			end
		end
		return lowest
	end

	def bid_by_user user
		return false unless user
		self.bid_projects.each do |bp|
			return true if bp.user_id == user.id
		end
		false
	end

end