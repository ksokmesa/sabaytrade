module UploadUtils
  require 'RMagick'

  def self.uploadSingleFile(key, value)
    img = Magick::Image::from_blob(value).first
    img.resize_to_fit!(200)
    path = "public/profile/"
    Dir.mkdir(path) unless File.exists?(path)
    img.write(path + key)
  end

  def self.uploadFile(fileHash)
    if fileHash
      fileHash.each do |key, value|
        img = Magick::Image::from_blob(Base64.decode64(value)).first
        img.resize_to_fit!(200)
        path = "public/profile/"
        Dir.mkdir(path) unless File.exists?(path)
        img.write(path + key)
      end
    end
  end

  def self.purgePhotos(photosToRemove)
    path = "public/profile/"
    photosToRemove.each { |photo|
      photoFileName = path + photo
      if File.exists?(photoFileName)
        File.delete(photoFileName)
      end
    }
  end

  def self.purgeUploadedPhotos(site)
    photoFields = Field.where(:collection_id => site.collection_id, :kind => 'photo')
    puts photoFields.length
    photoFields.each { |field|
      path = "public/profile/"
      photoFileName = site.properties[field.id.to_s]
      if !photoFileName.nil? and File.exists?(path + photoFileName)
          File.delete(path + photoFileName)
      end
    }

  end

end