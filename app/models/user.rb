class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  attr_accessible :email, :password, :password_confirmation, :type, :type_id
  belongs_to :type  
  has_many :bid_projects, dependent: :destroy
  has_many :projects, dependent: :destroy
  has_one :profile, dependent: :destroy
  after_create :create_profile


  def consult_projects
  	Project.where('consultant_id = ?', self.id)
  end

  def create_profile
    Profile.create!(:user => self)
  end

  def display_name
    if self.profile.first_name || self.profile.last_name
      return (self.profile.first_name || "") + " " + (self.profile.last_name || "")
    else
      return self.email
    end
  end

  def display_name_no_space
    if self.profile.first_name || self.profile.last_name
      return (self.profile.first_name || "") + (self.profile.last_name || "")
    else
      return self.email
    end
  end

  def is_employee?
    return self.type.name == "employee"
  end

  def is_freelancer?
    return self.type.name == "freelancer"
  end

  def display_anonymous_name
    n = self.display_name
    return n[0] + n[1] + "***********" + n[(n.size - 2)] + n[(n.size - 1)]
  end
end
