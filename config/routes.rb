Rails.application.routes.draw do
  devise_for :users
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
    root 'pages#index'
    match 'welcome' => 'pages#welcome', :via => :get
    match 'contact_us' => 'pages#contactus', :via => :get
    match 'about_us' => 'pages#aboutus', :via => :get
    match 'how_it_work' => 'pages#howitwork', :via => :get
    match 'be_an_employer' => 'pages#beanemployer', :via => :get
    match 'be_a_freelancer' => 'pages#beafreelancer', :via => :get
    match 'register_employer' => 'users#register_employer', :via => :get
    match 'register_freelancer' => 'users#register_freelancer', :via => :get
    match 'save_employer' => 'users#save_employer', :via => :post
    match 'save_freelancer' => 'users#save_freelancer', :via => :post
    match 'get_position' => 'users#get_position', :via => :get
    match 'freelancer/:name' => 'freelancers#view_profile', :via => :get
    match 'employer/:name' => 'employers#view_profile', :via => :get

    resources :users do
      collection do
        post 'register'
      end

      member do
        put 'update_password'
        get 'change_password'

      end
    end

    resources :projects do
      member do
        post 'post_bid'
        get 'detail'
        put 'assign_consultant'
      end
    end
    resources :contact_us
    resources :freelancers
    resources :employers

    namespace :admin do
      resources :freelancers
      resources :employers
      resources :projects do
        member do
          put 'assign_consultant'
        end
      end
      resources :internal_users do
        collection do 
          get 'my_profile'
          put 'update_password'
          get 'change_password'
        end
      end
    end

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
