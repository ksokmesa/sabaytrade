class AddTypeToUser < ActiveRecord::Migration
  def change
  	add_column :users, :type_id, :integer, references: :types
  end
end
