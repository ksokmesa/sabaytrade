class CreateProject < ActiveRecord::Migration
  def change
    create_table :projects do |t|
    	t.string  :name
    	t.text    :description
    	t.integer :budget
    	t.datetime    :start_date
    	t.datetime    :end_date
    	t.references :user

      t.timestamps
    end
  end
end
