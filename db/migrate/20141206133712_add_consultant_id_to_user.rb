class AddConsultantIdToUser < ActiveRecord::Migration
  def change
  	add_column :projects, :consultant_id, :integer
  	add_column :projects, :consultant_cost, :integer
  end
end
