class AddProfile < ActiveRecord::Migration
  def change
    create_table :profiles do |t|
    	t.string      :first_name
    	t.string      :last_name
      t.string      :sex
      t.text        :position
    	t.text        :photo_path
    	t.text        :skill
    	t.text        :address
    	t.string      :phone_number
    	t.date        :dob
    	t.text        :description
    	t.references  :user

      t.timestamps
    end
  end
end
