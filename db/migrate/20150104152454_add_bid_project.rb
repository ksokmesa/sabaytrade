class AddBidProject < ActiveRecord::Migration
  def change
  	create_table :bid_projects do |t|
    	t.references :user
    	t.references :project
    	t.integer    :cost
    	
      t.timestamps
    end
  end
end
