class RemoveIndustryFromFreelancerAndAddIndustryAndSubIndustryToProject < ActiveRecord::Migration
  def up
  	remove_column :profiles, :industry
    remove_column :profiles, :position
  	add_column :projects, :industry, :text
  	add_column :projects, :sub_industry, :text
  end

  def down
  	add_column :profiles, :industry, :text
    add_column :profiles, :position, :text
  	remove_column :projects, :industry
  	remove_column :projects, :sub_industry
  end
end
