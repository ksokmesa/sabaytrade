-- MySQL dump 10.13  Distrib 5.5.41, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: sabay_trade
-- ------------------------------------------------------
-- Server version	5.5.41-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `bid_projects`
--

DROP TABLE IF EXISTS `bid_projects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bid_projects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `cost` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bid_projects`
--

LOCK TABLES `bid_projects` WRITE;
/*!40000 ALTER TABLE `bid_projects` DISABLE KEYS */;
INSERT INTO `bid_projects` VALUES (16,26,8,19,'2015-03-04 05:57:37','2015-03-04 05:57:37'),(17,38,8,18,'2015-03-11 06:03:18','2015-03-11 06:03:18'),(18,38,13,12,'2015-03-11 06:03:37','2015-03-11 06:03:37'),(19,38,11,11,'2015-03-11 06:04:05','2015-03-11 06:04:05'),(20,39,8,17,'2015-03-11 08:44:53','2015-03-11 08:44:53'),(21,39,10,6,'2015-03-11 08:50:00','2015-03-11 08:50:00'),(22,45,13,11,'2015-03-13 05:02:39','2015-03-13 05:02:39'),(23,45,8,15,'2015-03-13 05:03:07','2015-03-13 05:03:07'),(24,45,11,10,'2015-03-13 05:03:46','2015-03-13 05:03:46'),(25,45,9,9,'2015-03-13 05:04:09','2015-03-13 05:04:09'),(26,45,12,14,'2015-03-13 05:04:53','2015-03-13 05:04:53');
/*!40000 ALTER TABLE `bid_projects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contact_us`
--

DROP TABLE IF EXISTS `contact_us`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contact_us` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `subject` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contact_us`
--

LOCK TABLES `contact_us` WRITE;
/*!40000 ALTER TABLE `contact_us` DISABLE KEYS */;
INSERT INTO `contact_us` VALUES (1,'sokmesa','khiev','ksokmesa@gmail.com','Getback','Hello','2014-11-28 14:33:57','2014-11-28 14:33:57'),(2,'Sakada','Sam','success.sak@gmail.com','how to bid project','hi','2015-02-01 02:16:57','2015-02-01 02:16:57');
/*!40000 ALTER TABLE `contact_us` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `profiles`
--

DROP TABLE IF EXISTS `profiles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `profiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sex` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `photo_path` text COLLATE utf8_unicode_ci,
  `skill` text COLLATE utf8_unicode_ci,
  `address` text COLLATE utf8_unicode_ci,
  `phone_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `user_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `company` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `profiles`
--

LOCK TABLES `profiles` WRITE;
/*!40000 ALTER TABLE `profiles` DISABLE KEYS */;
INSERT INTO `profiles` VALUES (3,'Sokmesa','Khiev','Male',NULL,'Ruby','Phnom Penh, Cambodia','85512657527','1983-05-03','Developer',3,'2014-12-23 14:48:18','2015-01-06 15:28:31',NULL),(22,'Sakada','Sam','Male','1425313597fortuneller .jpg','CopyWriting','Chroy Jong Va, Phnom Penh, Cambodia. ','098445168','1983-03-01','I am looking for the right people whom I can afford. ',22,'2015-03-02 16:14:03','2015-03-02 16:26:37','Global Compilation'),(23,'Uch','Sarath','Male','142531483610991428_10206208213585987_1805075670287505503_o.jpg','Web Development','#78E0z, St57, Sras Chork, Daun Penh, Phnom Penh','085 521-121','1990-12-01','I am a Ruby on Rails & Laravel developer with more than 2 years experience in software development. In particular, I also work in UX/UI design. Please let me know what I can help. ',23,'2015-03-02 16:39:17','2015-03-02 16:47:17',NULL),(24,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,24,'2015-03-02 16:41:17','2015-03-02 16:41:17',NULL),(25,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,25,'2015-03-02 16:45:08','2015-03-02 16:45:08',NULL),(26,'Sokmesa','Khiev','Male','1434596122IMG_1033.jpg','CopyWriting','Phnom Penh','85516222172','1987-04-03','Ruby on Rails and PHP developer',26,'2015-03-02 16:46:22','2015-06-18 02:55:23',NULL),(27,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,27,'2015-03-03 04:53:37','2015-03-03 04:53:37',NULL),(29,'David','Jason','Male','1425451696Smiling-Man-4.jpg','CopyWriting','New Zealand','0883456784','1971-12-01','I am an entrepreneur. I am traveling around the world. ',29,'2015-03-04 06:45:21','2015-03-04 06:48:16','Writing For Travelling'),(30,'William','Jackson','Male','1425452597BRIDES_ManOnTheStreet2_250.jpg','CopyWriting','New Zealand','0883456784','2015-03-18','I am interesting in technology and mobile phone. ',30,'2015-03-04 06:59:49','2015-03-04 07:03:17','CamTechnology'),(31,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,31,'2015-03-04 14:12:46','2015-03-04 14:12:46',NULL),(32,'Piseth','Sek','Male','14255314101.jpg','Translation','Phnom Penh City','0968442745','1981-07-24','I am a freelance translator from English to Khmer and vise versa since 1999. My area of specializations are translation including transcription and dubbing services in Khmer and English languages. Besides, I can also do graphic design, data mining, data entry, and copy typing.\r\n\r\nCurrently, I work as an Operation Manager for a Non-profit Organization. I got master degree in Development Management and graduated in Computer Science.\r\n\r\nI am also a level 2 seller on Fiverr @ www.fiverr.com/pisethz\r\n\r\nI interested in work that satisfies my clients. I not only seek to fulfill client requirements but also produce high quality products for them. Furthermore I am a discipline and high motivated person, and put high attention to details so I will deliver to you a perfect final result.',32,'2015-03-05 04:44:48','2015-03-05 04:56:50',NULL),(33,'Yoeung','Virakboth','Male','1425595712image.jpg','CopyWriting','#51, st 45BT','016770767','1983-03-12','Hello every one...',33,'2015-03-05 22:46:17','2015-03-05 22:48:32',NULL),(34,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,34,'2015-03-10 18:19:08','2015-03-10 18:19:08',NULL),(35,'Sokunthea','Seng','Female','1426029246image.jpg','CopyWriting','#456, st. 369, Sk. Chba Ampov II, Kh. Chba Ampov, Phnom Penh.','093560606','1991-04-03','Life is colorful when it exposes to many scenes in the society.  ',35,'2015-03-10 23:06:39','2015-03-10 23:14:06','Student of IFL '),(36,'Bella','Bloomfield','Female',NULL,'CopyWriting','36 Gamma st, Roslyn, Dunedin','0220784884','1992-12-04','I am currently finishing my Masters of Entrepreneurship Degree at the University of Otago in Dunedin, and have already completed my Bachelor of Product Design Degree.',36,'2015-03-11 02:15:31','2015-03-11 02:17:31',NULL),(37,'Meas','Kesor','Female',NULL,'CopyWriting','PP','012 340 042','1994-05-19','Student',37,'2015-03-11 04:29:19','2015-03-11 04:30:53',NULL),(38,'siven','chean','Female',NULL,'CopyWriting','Phnom penh','092657070','1993-01-01','Computer science engineer at Institute of Technology of Cambodia',38,'2015-03-11 05:56:38','2015-03-11 06:01:51',NULL),(39,'Chhayrotana','Prak','Female','1426056606_DSC0380.JPG','CopyWriting','Slorgram Commune, Siem Reap city, Cambodia ','012633541','1986-07-08','Hello, My name is Rotana, my Full name is Prak Chhayrotana. You can call me Tana in short. I had experience working with Interior Design Companies for many years, and product development training also become a trainer my self for the Cambodian Product design such as fabric silk and cotton. \r\n\r\nWhy I can write? I had been graduated from Institute of Foreign Language in International Business. Development of writing come by writing many speech for the former Head of Board of Engineers, Cambodia at the Council of Minister for most of his ASEAN Federation presentation and daily communication within the region in Southeast Asia for the past few year. \r\n\r\nInterest? \r\nI am experiencing creativity in art. I do love history, and many other story to the related fields. It is including everyday lifestyle of artist; photographer, filmmaker, singer, writer painter, tattoo artist for example. I also do love traveling into many greatest high end resort and hotel as most of it mostly having the greatest view of art display included soft, scripture, painting. \r\n\r\nPurpose?\r\nI do believe that Siem Reap is the place where we Cambodia having a lot to say about. So many stories are going on here full of life and joy, which is related more, into art and tourism if we say so to the tourism. But more than that, the activities which the local love to do around here needs to be told. They are so much great people who love to talk to us who live the entire country and to the world if we do publish this. \r\n\r\nI hope it is possible if I can start writing at least a few article per week. \r\n\r\nHope this will be a great start to trade my skill here. \r\n\r\nSiem Reap With love, \r\nTana\r\n',39,'2015-03-11 06:08:14','2015-03-11 06:50:07',NULL),(40,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,40,'2015-03-11 06:23:10','2015-03-11 06:23:10',NULL),(41,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,41,'2015-03-11 07:24:23','2015-03-11 07:24:23',NULL),(42,'Chheng ','Chandy ','Female',NULL,'CopyWriting','#215, st. 138, Phnom Penh ','+855 12 936251 ','1990-06-06','I has establish my business in 2015 and I hope it can progress early in year also ',42,'2015-03-11 08:02:13','2015-03-11 08:04:28',NULL),(43,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,43,'2015-03-11 12:26:07','2015-03-11 12:26:07',NULL),(44,'sreynak','chet','Female',NULL,'CopyWriting','Phnom Penh','015671731','1993-02-02','I am sreynak',44,'2015-03-11 13:19:06','2015-03-11 13:32:09',NULL),(45,'Jessica','Sean','Female','1426222868Female-Executive_3.jpg','CopyWriting','19 constitution st.','+64212069430','2015-03-13','I am a freelancer in New Zealand. ',45,'2015-03-13 04:58:05','2015-03-13 05:01:08',NULL),(46,'kheang','seng','Male',NULL,'CopyWriting','No# 57, chaom chao','010491618','1985-04-22','Accounting, Finance and Auditing',46,'2015-04-22 04:48:19','2015-04-22 04:50:03',NULL),(47,'Somaly','Keo','Female','1430808734Hydrangeas.jpg','CopyWriting','Phnom Penh','0979900190','1987-01-19','Translating English and Thai ',47,'2015-05-05 06:45:43','2015-05-05 06:52:14',NULL);
/*!40000 ALTER TABLE `profiles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_types`
--

DROP TABLE IF EXISTS `project_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_types`
--

LOCK TABLES `project_types` WRITE;
/*!40000 ALTER TABLE `project_types` DISABLE KEYS */;
INSERT INTO `project_types` VALUES (1,'CopyWriting','2014-12-01 15:30:04','2015-02-09 13:58:36'),(2,'Web Development','2014-12-01 15:30:20','2015-02-09 13:59:58'),(3,'Translation','2014-12-01 15:32:44','2015-02-09 13:59:27'),(4,'Graphic Design and ICT Materials','2015-02-09 14:00:34','2015-02-09 14:00:34');
/*!40000 ALTER TABLE `project_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projects`
--

DROP TABLE IF EXISTS `projects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `budget` int(11) DEFAULT NULL,
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `project_type_id` int(11) DEFAULT NULL,
  `consultant_id` int(11) DEFAULT NULL,
  `consultant_cost` int(11) DEFAULT NULL,
  `tags` text COLLATE utf8_unicode_ci,
  `industry` text COLLATE utf8_unicode_ci,
  `sub_industry` text COLLATE utf8_unicode_ci,
  `status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects`
--

LOCK TABLES `projects` WRITE;
/*!40000 ALTER TABLE `projects` DISABLE KEYS */;
INSERT INTO `projects` VALUES (8,'Beautiful Places ','Number of Articles: 3 (100 words for each articles)\r\n1- The Most beautiful place to visit in Vietnam\r\n2- The Most beautiful place to visit Malaysia\r\n3- The most beautiful place in Indonesia',20,'2015-03-02 16:23:12','2015-03-21 00:00:00',22,'2015-03-02 16:23:12','2015-03-04 06:44:32',NULL,NULL,NULL,'Writing','CopyWriting','Marketing Article','Bidding'),(9,'Article: What is the most famous food in Cambodia? ','I am looking for someone, specially students who are able to write a three-paragraph-essay about amazing foods in Cambodia. There should be about 250 - 300 words for all the three paragraph. ',10,'2015-03-04 06:57:14','2015-03-20 00:00:00',29,'2015-03-04 06:57:14','2015-03-11 04:32:14',NULL,NULL,NULL,'writing ','CopyWriting','Marketing Article','Bidding'),(10,'Why Kids should not use Mobile Phone?','One article about 300 words. \r\nTell me 5 reasons why parents should never allow their kids to use mobile phone. ',7,'2015-03-04 07:05:38','2015-03-20 00:00:00',30,'2015-03-04 07:05:38','2015-03-04 07:05:38',NULL,NULL,NULL,'writing','CopyWriting','Marketing Article','Bidding'),(11,'What are top 5 reasons why girls should marry a Cambodian man? ','Length: 300 - 350 words\r\nOriginal Content',12,'2015-03-04 07:14:58','2015-03-27 00:00:00',29,'2015-03-04 07:14:58','2015-03-12 02:44:21',NULL,NULL,NULL,'writing','CopyWriting','Marketing Article','Bidding'),(12,'What are top 5 reasons why a foreigner should marry a Cambodian girl? ','Length: 400 - 500\r\nOriginal Content',15,'2015-03-04 07:18:35','2015-03-25 00:00:00',29,'2015-03-04 07:18:35','2015-03-11 04:32:31',NULL,NULL,NULL,'writing','CopyWriting','Marketing Article','Bidding'),(13,'Why girls should not use whitening?','Length: 300 - 350\r\nOriginal content\r\nCreative and Funny',13,'2015-03-04 07:29:35','2015-03-17 00:00:00',29,'2015-03-04 07:29:35','2015-03-04 07:29:35',NULL,NULL,NULL,'writing','CopyWriting','Marketing Article','Bidding'),(14,'the best place to visit in Myanmar','This article must use two main key words: best place, Myanmar. \r\nLength: 300 - 500\r\n',10,'2015-03-13 05:26:58','2015-03-24 00:00:00',30,'2015-03-13 05:26:58','2015-03-13 05:26:58',NULL,NULL,NULL,'writing','CopyWriting','Marketing Article','Bidding');
/*!40000 ALTER TABLE `projects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `schema_migrations`
--

DROP TABLE IF EXISTS `schema_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schema_migrations` (
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  UNIQUE KEY `unique_schema_migrations` (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schema_migrations`
--

LOCK TABLES `schema_migrations` WRITE;
/*!40000 ALTER TABLE `schema_migrations` DISABLE KEYS */;
INSERT INTO `schema_migrations` VALUES ('20141124152012'),('20141128131714'),('20141128135650'),('20141128140034'),('20141129011900'),('20141201152042'),('20141201152145'),('20141206133712'),('20141222133019'),('20150102024930'),('20150104152454'),('20150209141743'),('20150224140209'),('20150224141608'),('20150301022703');
/*!40000 ALTER TABLE `schema_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `types`
--

DROP TABLE IF EXISTS `types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `types`
--

LOCK TABLES `types` WRITE;
/*!40000 ALTER TABLE `types` DISABLE KEYS */;
INSERT INTO `types` VALUES (1,'freelancer','2014-11-28 13:59:48','2014-11-28 13:59:48'),(2,'employer','2014-11-28 13:59:53','2014-11-28 13:59:53'),(3,'admin','2014-12-18 14:14:59','2014-12-18 14:14:59');
/*!40000 ALTER TABLE `types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `encrypted_password` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `reset_password_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reset_password_sent_at` datetime DEFAULT NULL,
  `remember_created_at` datetime DEFAULT NULL,
  `sign_in_count` int(11) NOT NULL DEFAULT '0',
  `current_sign_in_at` datetime DEFAULT NULL,
  `last_sign_in_at` datetime DEFAULT NULL,
  `current_sign_in_ip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_sign_in_ip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `confirmation_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `confirmed_at` datetime DEFAULT NULL,
  `confirmation_sent_at` datetime DEFAULT NULL,
  `unconfirmed_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_users_on_email` (`email`),
  UNIQUE KEY `index_users_on_reset_password_token` (`reset_password_token`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (3,'success.sak@gmail.com','$2a$10$mZOjD5R2qA73K8chkojUO.G.32ooHTwH4EauFdKZW54ClCa0m/xAS',NULL,NULL,NULL,64,'2015-04-13 04:32:20','2015-03-14 03:04:41','122.62.153.202','36.37.153.163',NULL,NULL,NULL,NULL,'2014-12-18 14:24:19','2015-04-13 04:32:20',3),(22,'sakada_sam@yahoo.com','$2a$10$dRxv269Z8MZiXT67/gj5n.FISJsrQ5U/jJi20hdcBhCmgS8B9Jhd.',NULL,NULL,NULL,6,'2015-03-04 06:43:51','2015-03-04 06:00:33','42.115.121.191','42.115.121.191',NULL,NULL,NULL,NULL,'2015-03-02 16:14:02','2015-03-04 06:43:51',2),(23,'sarath7khmer@gmail.com','$2a$10$qBsFO/qc/6lnuyBCIrmhGuViTENThhTXJzBpJaNYDwXh.gGMSDpQy',NULL,NULL,NULL,1,'2015-03-02 16:39:17','2015-03-02 16:39:17','114.134.189.190','114.134.189.190',NULL,NULL,NULL,NULL,'2015-03-02 16:39:17','2015-03-02 16:39:17',1),(24,'chantra.be@gmail.com','$2a$10$129MtwLnAFc5FjF00v3FFepLLYnp9.PVmGV1mPM1o926WjZlxzn9y',NULL,NULL,NULL,1,'2015-03-02 16:41:17','2015-03-02 16:41:17','42.115.101.124','42.115.101.124',NULL,NULL,NULL,NULL,'2015-03-02 16:41:17','2015-03-02 16:41:17',1),(25,'sophananhor27@gmail.com','$2a$10$ZEnCMhohFK210Djz0Px.Nu0nEr/DG/GWOBbv0l.ygOMtNXzP5uGg.',NULL,NULL,NULL,1,'2015-03-02 16:45:08','2015-03-02 16:45:08','202.58.99.18','202.58.99.18',NULL,NULL,NULL,NULL,'2015-03-02 16:45:08','2015-03-02 16:45:08',1),(26,'mesa@instedd.org','$2a$10$Q7TpnReAXM06aPH9Q0Xth.119j8BaJ86gtZfQ1GspOmwdW0eJVeM.',NULL,NULL,NULL,7,'2015-06-18 02:54:56','2015-03-25 10:08:10','110.74.204.121','110.74.204.121',NULL,NULL,NULL,NULL,'2015-03-02 16:46:22','2015-06-18 02:54:56',1),(27,'cheamsreyaun@ymail.com','$2a$10$kuNU.HxIbR11gnrRXcwUTezT2VHbnQtpCUbiJ/rONYsOXrdBQcEhe',NULL,NULL,NULL,3,'2015-03-10 01:37:32','2015-03-03 05:00:45','203.144.93.213','175.100.10.210',NULL,NULL,NULL,NULL,'2015-03-03 04:53:37','2015-03-10 01:37:32',1),(29,'globalcompilation@gmail.com','$2a$10$tMycwWcwEMvW0AcQpttWI.WeIFkMIu5NElKaSCLBDpwHRwIr9i1k2',NULL,NULL,NULL,4,'2015-03-13 05:24:21','2015-03-04 07:11:14','42.115.54.160','42.115.121.191',NULL,NULL,NULL,NULL,'2015-03-04 06:45:21','2015-03-13 05:24:21',2),(30,'my999movies@gmail.com','$2a$10$ppa3h/hGiMw9iBcOf4gruuYamyScPu6hKwyi/bIJz3L.khKywRTnW',NULL,NULL,NULL,2,'2015-03-13 05:24:44','2015-03-04 06:59:49','42.115.54.160','42.115.121.191',NULL,NULL,NULL,NULL,'2015-03-04 06:59:49','2015-03-13 05:24:44',2),(31,'lachvannak@gmail.com','$2a$10$/12vhNvgdrJ4auiYKUvdUuaCHEKHFA94M8AvqDOf8eIJfOnV.Fy2G',NULL,NULL,NULL,1,'2015-03-04 14:12:46','2015-03-04 14:12:46','42.115.121.60','42.115.121.60',NULL,NULL,NULL,NULL,'2015-03-04 14:12:46','2015-03-04 14:12:46',2),(32,'techsek@gmail.com','$2a$10$ls9tYhEz6LZnuuYBjmVqguEzSmwB5pRjVvGlL8yfUo0G8PEUCa1xO',NULL,NULL,NULL,1,'2015-03-05 04:44:48','2015-03-05 04:44:48','110.74.210.218','110.74.210.218',NULL,NULL,NULL,NULL,'2015-03-05 04:44:48','2015-03-05 04:44:48',1),(33,'yoeungboth@yahoo.com','$2a$10$KLQ.elqRH36tRwiCpjtro.d.hGh3h7.V60cowrZLmfuRfKK/bsQWy',NULL,NULL,NULL,1,'2015-03-05 22:46:17','2015-03-05 22:46:17','203.189.134.198','203.189.134.198',NULL,NULL,NULL,NULL,'2015-03-05 22:46:17','2015-03-05 22:46:17',1),(34,'cheadalin90@gmail.com','$2a$10$sBAEyuFLuvvCsYfalUAItej4Alpa8L./w55dKwh5szOkG4lOdHpMC',NULL,NULL,NULL,1,'2015-03-10 18:19:08','2015-03-10 18:19:08','103.253.181.137','103.253.181.137',NULL,NULL,NULL,NULL,'2015-03-10 18:19:08','2015-03-10 18:19:08',1),(35,'sengsokunthea1@gmail.com','$2a$10$b8DPwo1Zaei38ftZmOfYieJeSSc0ymDl0CSIcIrALp4g0Xdbg0W/.',NULL,NULL,NULL,1,'2015-03-10 23:06:39','2015-03-10 23:06:39','203.144.93.127','203.144.93.127',NULL,NULL,NULL,NULL,'2015-03-10 23:06:39','2015-03-10 23:06:39',2),(36,'bellabloomfield04@gmail.com','$2a$10$YRgIFqfmroVuk89jM.f0kuR8royUQBHONfhKjxs4g0epC6DM4BCpO',NULL,NULL,NULL,1,'2015-03-11 02:15:31','2015-03-11 02:15:31','118.92.80.85','118.92.80.85',NULL,NULL,NULL,NULL,'2015-03-11 02:15:31','2015-03-11 02:15:31',1),(37,'iluvshare2@gmail.com','$2a$10$l0LV3R4ZnpvC1OeriaVv0OvK9dkI.cXNtJh8oldK52SJUkBIZ/SAa',NULL,NULL,NULL,1,'2015-03-11 04:29:19','2015-03-11 04:29:19','119.82.252.95','119.82.252.95',NULL,NULL,NULL,NULL,'2015-03-11 04:29:19','2015-03-11 04:29:19',1),(38,'cheansiven@gmail.com','$2a$10$QdaBh5femOvtIJdNNhkhmOjqQ6SQXhACxljpYnTip/Jo3y0wDZWr.',NULL,NULL,NULL,1,'2015-03-11 05:56:38','2015-03-11 05:56:38','103.12.160.179','103.12.160.179',NULL,NULL,NULL,NULL,'2015-03-11 05:56:38','2015-03-11 05:56:38',1),(39,'rotana.designer@gmail.com','$2a$10$DkrpN4FluuUcoxXupOqV7.SboJyDeodhUc5RyoW9WD8EjN18GOWE2',NULL,NULL,NULL,1,'2015-03-11 06:08:14','2015-03-11 06:08:14','180.178.127.4','180.178.127.4',NULL,NULL,NULL,NULL,'2015-03-11 06:08:14','2015-03-11 06:08:14',1),(40,'chhoun.samnang89@yahoo.com','$2a$10$rwkUYhruQzhVu5uUcHKzV.BdBbBnCzVzGkIxEKFPYixeYUce4wxs2',NULL,NULL,NULL,1,'2015-03-11 06:23:10','2015-03-11 06:23:10','114.134.185.196','114.134.185.196',NULL,NULL,NULL,NULL,'2015-03-11 06:23:10','2015-03-11 06:23:10',2),(41,'sreyna_lim@yahoo.com','$2a$10$ERydp83KeX189.wgj/Z6cuhIcmElh/tKY44hg7t4HmgHz/AIh5TRy',NULL,NULL,NULL,1,'2015-03-11 07:24:23','2015-03-11 07:24:23','202.79.26.53','202.79.26.53',NULL,NULL,NULL,NULL,'2015-03-11 07:24:23','2015-03-11 07:24:23',2),(42,'chhengchandy@gmail.com','$2a$10$iTWzvIzazxYRxSWXHlGxxOnhLr1ouDmIKfgeHcelC7k/qjwNsM9jW',NULL,NULL,NULL,1,'2015-03-11 08:02:13','2015-03-11 08:02:13','42.115.41.119','42.115.41.119',NULL,NULL,NULL,NULL,'2015-03-11 08:02:13','2015-03-11 08:02:13',1),(43,'chhunlin_ly@ymail.com','$2a$10$Op3Yq0oykoqyzRi6JYxQTuWXgs05bgpjPNxNUfU6cixuzkIEiZYYq',NULL,NULL,NULL,3,'2015-03-11 14:21:47','2015-03-11 14:11:40','203.144.92.84','203.144.92.84',NULL,NULL,NULL,NULL,'2015-03-11 12:26:07','2015-03-11 14:21:47',1),(44,'nakpnn.chet05@gmail.com','$2a$10$b30FFQdokaJNo37qHngzmuwaqXR/HqHayGykK9FvnSlm0JAj8y5vy',NULL,NULL,NULL,1,'2015-03-11 13:19:06','2015-03-11 13:19:06','36.37.216.155','36.37.216.155',NULL,NULL,NULL,NULL,'2015-03-11 13:19:06','2015-03-11 13:19:06',1),(45,'top10fashionista@gmail.com','$2a$10$LQD0jF8kXILtyQZGyVgR7eY4fcw4GjLBN6Ohb0i5oEjCEZFq/FW2e',NULL,NULL,NULL,1,'2015-03-13 04:58:05','2015-03-13 04:58:05','42.115.54.160','42.115.54.160',NULL,NULL,NULL,NULL,'2015-03-13 04:58:05','2015-03-13 04:58:05',1),(46,'sengkheangte@gmail.com','$2a$10$Hy.mc1qZ.DrBjKRS91hC/Oc32zkL5TtYNy3rqhHtgi7BxiOhOjK3q',NULL,NULL,NULL,1,'2015-04-22 04:48:19','2015-04-22 04:48:19','103.9.188.142','103.9.188.142',NULL,NULL,NULL,NULL,'2015-04-22 04:48:19','2015-04-22 04:48:19',1),(47,'keomalylila@gmail.com','$2a$10$4OD0mZ03xFXdPq4AmoVnnu7og6yLMvktwlzpz39L0D4m7HPYHqYdK',NULL,NULL,NULL,2,'2015-06-04 06:55:30','2015-05-05 06:45:43','124.248.177.40','124.248.175.225',NULL,NULL,NULL,NULL,'2015-05-05 06:45:43','2015-06-04 06:55:30',1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-07-11  6:46:11
